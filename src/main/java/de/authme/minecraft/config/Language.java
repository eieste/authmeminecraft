package de.authme.minecraft.config;

import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;

public class Language {

    private static final HashMap<Locale, ResourceBundle> languages = new HashMap<>();

    public static ResourceBundle getResourceBunde(Locale locale){
        if(!languages.containsKey(locale))
            languages.put(locale, ResourceBundle.getBundle("language.Language", locale));

        return languages.get(locale);
    }

}
