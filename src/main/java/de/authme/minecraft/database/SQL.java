package de.authme.minecraft.database;

import de.authme.minecraft.AuthMeMinecraft;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SQL {
	private Connection a;
	private Statement b;
	private List<String> d = new ArrayList<>();
	
	public SQL(String host, String user, String pass, String db, String port){
		d.add(host);
		d.add(user);
		d.add(pass);
		d.add(db);
		d.add(port);
		this.connect();
	}
	
	private void connect(){
		System.out.println("Connect to MySQL: "+d.get(1)+"@"+d.get(0)+":"+d.get(4)+"/"+d.get(3));
		try{
			Class.forName("com.mysql.jdbc.Driver");
			a = DriverManager.getConnection("jdbc:mysql://"+d.get(0)+ ":"+d.get(4)+"/"+d.get(3), d.get(1), d.get(2));
		}
		catch (Exception e) {
            AuthMeMinecraft.getInstance().error(e);
		}
	}
	
	public ResultSet query(String x){
		try {
			return a.createStatement().executeQuery(x);
		} catch (SQLException e) {
            AuthMeMinecraft.getInstance().error(e);
			return null;
		}
	}

	public ResultSet query(String x, Object...args){
		try {
			PreparedStatement prepStmt = a.prepareStatement(x);
			for(int y=0;y<args.length;y++){
				if(args[y].getClass().equals(String.class)) prepStmt.setString(y+1, (String)args[y]);
				if(args[y].getClass().equals(Integer.class)) prepStmt.setInt(y+1, (Integer)args[y]);
				if(args[y].getClass().equals(Long.class)) prepStmt.setLong(y+1, (Long)args[y]);
			}
			return prepStmt.executeQuery();
		} catch (SQLException e) {
            AuthMeMinecraft.getInstance().error(e);
			return null;
		}
	}
	
	public int update(String x){
		try {
			return a.createStatement().executeUpdate(x);
		} catch (SQLException e) {
            AuthMeMinecraft.getInstance().error(e);
			return 0;
		}
	}
	
	public int update(String x, Object...args){
		try {
			PreparedStatement prepStmt = a.prepareStatement(x);
			for(int y=0;y<args.length;y++)
                prepStmt.setObject(y+1, args[y]);

			return prepStmt.executeUpdate();
		} catch (SQLException e) {
            AuthMeMinecraft.getInstance().error(e);
			return 0;
		}
	}
	
	public void renew(String table){
		try {
			if(this.a.isClosed()){
				this.connect();
			}
            this.query("SELECT * FROM `"+table+"` LIMIT 1").close();
		} catch (SQLException e) {
            AuthMeMinecraft.getInstance().error(e);
		}
	}
	
	
}
