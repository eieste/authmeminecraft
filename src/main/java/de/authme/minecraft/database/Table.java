package de.authme.minecraft.database;

public enum Table {

    MC_VALIDATION_WHITELIST("mc_validation_whitelist"),
    MC_VALIDATION_LOG("mc_validation_log");

    private final String name;

    private Table(String name){
        this.name = name;
    }

    public String getColumn(Column column){
        return column.toString();
    }

    @Override
    public String toString(){
        return this.name;
    }

    public interface Column{}

    public enum MC_VALIDATION_WHITELIST_Column implements Column{

        ID("id"),
        WEB_MC_NAME("web_mc_name"),
        WEB_MC_UUID("web_mc_uuid"),
        WEB_INSERT_TIME("web_insert_timestamp"),
        JOIN_TIMESTAMP("join_timestamp"),
        KICK_TIMESTAMP("kick_timestamp"),
        FLAG_VALIDATION("flag_for_validation"),
        FLAG_ON_SERVER("flag_at_the_server"),
        FLAG_VALID("flag_valid"),
        FLAG_FINISH("flag_finish_process"),
        FLAG_DELETE("flag_delete"),
        AKTIV("aktiv");

        private final String name;

        private MC_VALIDATION_WHITELIST_Column(String name){
            this.name = name;
        }

        @Override
        public String toString(){
            return this.name;
        }
    }

    public enum MC_VALIDATION_LOG_Column implements Column{

        ID("id"),
        MC_VALIDATION_WHITELIST_ID("mc_validation_whitelist_id"),
        MC_UUID("mc_uuid"),
        IP("user_ip"),
        TIMESTAMP("timestamp"),
        FLAG_DELETE("flag_deleted"),
        AKTIV("aktiv");

        private final String name;

        private MC_VALIDATION_LOG_Column(String name){
            this.name = name;
        }

        @Override
        public String toString(){
            return this.name;
        }
    }
}
