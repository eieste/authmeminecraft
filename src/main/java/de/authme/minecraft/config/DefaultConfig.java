package de.authme.minecraft.config;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class DefaultConfig {
	
	private final JavaPlugin a;
	private final YamlConfiguration b;
	
	public DefaultConfig(JavaPlugin javaPlugin, YamlConfiguration defaultconfig){
		this.a = javaPlugin;
		this.b = defaultconfig;
		this.check();
	}
	
	public void check(){
        assert this.a != null;
        assert this.b != null;
		for(String key: this.b.getKeys(false)){
			if(!this.a.getConfig().contains(key)) this.a.getConfig().set(key, this.b.get(key));
		}
		this.a.saveConfig();
	}
	
}
