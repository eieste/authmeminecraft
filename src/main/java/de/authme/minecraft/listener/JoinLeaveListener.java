package de.authme.minecraft.listener;

import de.authme.minecraft.AuthMeMinecraft;
import de.authme.minecraft.config.Language;
import de.authme.minecraft.database.Table;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

public class JoinLeaveListener implements Listener{

    @EventHandler
    public void prelogin(AsyncPlayerPreLoginEvent e){

        UUID uuid = e.getUniqueId();

        int whitelist_id = -1;

        ResultSet resultSet = AuthMeMinecraft.getInstance().getSql().query("SELECT * FROM `"+ Table.MC_VALIDATION_WHITELIST.toString()+"` WHERE `"+Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.WEB_MC_UUID)+"`=? AND `"+Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.AKTIV)+"`='1' AND `"+Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.FLAG_DELETE)+"`='0' AND `"+Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.FLAG_FINISH)+"`='0' LIMIT 1", uuid.toString().replace("-", ""));
        try{
            if(resultSet.next()){
                whitelist_id = resultSet.getInt(Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.ID));
            }
        }catch(Exception e1){
            AuthMeMinecraft.getInstance().error(e1);
        }

        if(whitelist_id == -1)
            e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_WHITELIST, Language.getResourceBunde(Locale.ENGLISH).getString("not_whitelist"));

        AuthMeMinecraft.getInstance().getSql().update("INSERT INTO `"+Table.MC_VALIDATION_LOG.toString()+"`(`"+ Table.MC_VALIDATION_LOG.getColumn(Table.MC_VALIDATION_LOG_Column.MC_VALIDATION_WHITELIST_ID)+"`,`"+Table.MC_VALIDATION_LOG.getColumn(Table.MC_VALIDATION_LOG_Column.MC_UUID)+"`,`"+Table.MC_VALIDATION_LOG.getColumn(Table.MC_VALIDATION_LOG_Column.IP)+"`,`"+Table.MC_VALIDATION_LOG.getColumn(Table.MC_VALIDATION_LOG_Column.TIMESTAMP)+"`,`"+ Table.MC_VALIDATION_LOG.getColumn(Table.MC_VALIDATION_LOG_Column.FLAG_DELETE)+"`, `"+ Table.MC_VALIDATION_LOG.getColumn(Table.MC_VALIDATION_LOG_Column.AKTIV)+"`) VALUES (?,?,?,?,?,?)", whitelist_id,uuid.toString().replace("-", ""),e.getAddress().getHostAddress(), new Timestamp(Calendar.getInstance(Locale.GERMAN).getTime().getTime()), 0, 1);
    }

    @EventHandler
    public void login(PlayerLoginEvent e) {
        UUID uuid = e.getPlayer().getUniqueId();

        int whitelist_id = -1;

        ResultSet resultSet = AuthMeMinecraft.getInstance().getSql().query("SELECT * FROM `"+ Table.MC_VALIDATION_WHITELIST.toString()+"` WHERE `"+Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.WEB_MC_UUID)+"`=? AND `"+Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.AKTIV)+"`='1' AND (`"+Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.FLAG_DELETE)+"`='0' OR `"+Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.FLAG_FINISH)+"`='0') LIMIT 1", uuid.toString().replace("-", ""));
        try{
            if(resultSet.next()){
                whitelist_id = resultSet.getInt(Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.ID));
            }
        }catch(Exception e1){
            AuthMeMinecraft.getInstance().error(e1);
        }

        if(whitelist_id == -1){
            e.disallow(PlayerLoginEvent.Result.KICK_BANNED, Language.getResourceBunde(Locale.forLanguageTag(e.getPlayer().spigot().getLocale())).getString("hack_over"));
            return;
        }

        e.getPlayer().setMetadata("db_id", new FixedMetadataValue(AuthMeMinecraft.getInstance(), whitelist_id));

        AuthMeMinecraft.getInstance().getSql().update("UPDATE `"+ Table.MC_VALIDATION_WHITELIST.toString()+"` SET `"+Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.JOIN_TIMESTAMP)+"`=?,`"+Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.FLAG_VALIDATION)+"`=?,`"+Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.FLAG_ON_SERVER)+"`=?,`"+Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.FLAG_VALID)+"`=? WHERE `"+ Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.ID)+"`=?", new Timestamp(Calendar.getInstance(Locale.GERMAN).getTime().getTime()), 0, 1, 1, whitelist_id);
    }

    @EventHandler
    public void join(PlayerJoinEvent e){
        e.setJoinMessage(null);
    }

    @EventHandler
    public void left(PlayerQuitEvent e){
        e.setQuitMessage(null);
        if(e.getPlayer().hasMetadata("db_id"))
            AuthMeMinecraft.getInstance().getSql().update("UPDATE `"+ Table.MC_VALIDATION_WHITELIST.toString()+"` SET `"+Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.KICK_TIMESTAMP)+"`=?,`"+Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.FLAG_ON_SERVER)+"`=?,`"+Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.FLAG_FINISH)+"`=? WHERE `"+ Table.MC_VALIDATION_WHITELIST.getColumn(Table.MC_VALIDATION_WHITELIST_Column.ID)+"`=?", new Timestamp(Calendar.getInstance(Locale.GERMAN).getTime().getTime()), 0, 1, e.getPlayer().getMetadata("db_id").get(0).asInt());
    }

}
