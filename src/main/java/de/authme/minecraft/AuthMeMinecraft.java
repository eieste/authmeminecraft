package de.authme.minecraft;

import de.authme.minecraft.config.DefaultConfig;
import de.authme.minecraft.database.SQL;
import de.authme.minecraft.listener.JoinLeaveListener;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class AuthMeMinecraft extends JavaPlugin {

    @Getter
    private static AuthMeMinecraft instance;

    @Getter
    private SQL sql;

    @Override
    public void onEnable() {
        instance = this;

        new DefaultConfig(this, YamlConfiguration.loadConfiguration(getResource("config.yml")));
        this.sql = new SQL(getConfig().getString("mysql.host"), getConfig().getString("mysql.user"), getConfig().getString("mysql.pass"), getConfig().getString("mysql.db"), getConfig().getString("mysql.port", "3306"));
        Bukkit.getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                AuthMeMinecraft.this.sql.renew("mc_validation_whitelist");
            }
        }, getConfig().getLong("mysql.keep-alive", 14400)*20L, getConfig().getLong("mysql.keep-alive", 14400)*20L);

        getServer().getPluginManager().registerEvents(new JoinLeaveListener(), this);
    }

    public void error(Exception e){
        e.printStackTrace();
    }

    public void info(String message){
        getLogger().info("["+getDescription().getName()+"] "+message);
    }
}
